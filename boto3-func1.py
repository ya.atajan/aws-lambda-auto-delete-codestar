import boto3
from datetime import datetime, timedelta, timezone

# 24 hours limit from lambda invocation
timeLimit = (datetime.now(timezone.utc) - timedelta(hours=24))

# Create an Codestar client
client = boto3.client('codestar', region_name='us-east-1')

# begins lambda function
def lambda_handler(event, context):

# Call Codestar to list current projects
response = client.list_projects()

# Get a list of all project ids from the response
projectIds = [projects['projectId'] for projects in response['projects']]

# Iterate through projects
for projectId in projectIds:

        # Gets time stamp for each project
        description = client.describe_project(id=projectId)

        # If a project is older than 24 hours, then the project is deleted
        if description['createdTimeStamp'] < timeLimit:
            response = client.delete_project(id=projectId, deleteStack=True)