import boto3
import re
from datetime import datetime, timedelta, timezone

# Create clients
codestar = boto3.client('codestar', region_name='us-east-1')
iam = boto3.client('iam')
ses = boto3.client('ses')


# begins lambda function
def lambda_handler(event, context):
    # List to store all temporary user ARNs and customer story ids
    temp_arns = []
    temp_story_ids = []

    # Time limit for stories and users to be deleted
    time_limit = (datetime.now(timezone.utc) - timedelta(hours=24))

    # Regex to extract username from userARN
    regex = re.compile('[^/]+(?=/$|$)')

    # List current projects
    list_projects = codestar.list_projects()

    # Get a list of all project ids that start with ...
    project_ids = [projects['projectId'] for projects in list_projects['projects']]

    for project_id in project_ids:

        if '...' in project_id:
            temp_story_ids.append(project_id)

    # List current temp users in the path /.../
    list_users = iam.list_users(PathPrefix='/.../')

    # Get a list of all ARNs from the response and pick-out temp users older than 24 hours
    user_arns = [arns['Arn'] for arns in list_users['Users']]

    for user_arn in user_arns:
        user_name = regex.search(user_arn).group()
        get_user = iam.get_user(UserName=user_name)

        # TODO: fix > to <
        if get_user['User']['CreateDate'] >= time_limit:
            temp_arns.append(user_arn)

    # List team members in a project
    for project_id in temp_story_ids:
        list_team_members = codestar.list_team_members(projectId=project_id)
        team_member_arns = [arns['userArn'] for arns in list_team_members['teamMembers']]

        # If a project contains s temp user older than 24 hours, it will delete the project as well as the temp user
        if set(temp_arns).intersection(team_member_arns):
            # User name of the temp user to be deleted
            user_name = regex.search(temp_arns[0]).group()

            # Exception handlers for NoSuchEntityException
            try:
                delete_login_profile = iam.delete_login_profile(UserName=user_name)
                print('Deleted the log in profile of ' + user_name)
            except iam.exceptions.NoSuchEntityException:
                print('Log in profile does not exist for user' + user_name)

            try:
                disassociate_team_member = codestar.disassociate_team_member(projectId=project_id, userArn=temp_arns[0])
                print(user_name + ' removed from group')
            except codestar.exceptions.NoSuchEntityException:
                print(user_name + ' is not a member of the project ' + project_id)

            try:
                delete_user_policy = iam.delete_user_policy(UserName=user_name, PolicyName='...')
                print('Deleted the user policy of ' + user_name)
            except iam.exceptions.NoSuchEntityException:
                print(user_name + ' does not have policy ...')

            try:
                delete_user = iam.delete_user(UserName=user_name)
                print('Deleted the user ' + user_name)
            except iam.exceptions.NoSuchEntityException:
                print('There is no user named ' + user_name)

            try:
                delete_project = codestar.delete_project(id=project_id, deleteStack=True)
                print('Deleted project: ' + project_id)
            except codestar.exceptions.NoSuchEntityException:
                print('There is no project with ID ' + project_id)
